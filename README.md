# Portafolio Kotlin Compose

For this project, I will make a very simple portfolio example, to understand a little more about the structure and facilities that Jetpack Compose offers to develop directly with Kotlin.

## Project Summary

This is a short, introductory practice on how to use Jetpack Compose, initializing the project with a Composable type screen, which will allow me to use annotations and declarations to use some basic components, including LazyColumns.


![image](/markdown/img1.png)


In addition, to know how to get the best out of the android Studio tools, understand and understand the use of the layer inspector, the hierarchy and inheritance of composable methods, the previewer that is also very helpful to render composable components.

![image](/markdown/img2.png)

## project download
```
git clone https://gitlab.com/android-kotlin3/portafolio-kotlin-compose.git
```

## Authors and acknowledgment
Max Herrera.

## License
For open source projects, say how it is licensed.

## Solution to present:

An important detail is that the project was carried out with Kotlin for the development and management of the interface state, through the Jetpack Compose library.


***
![](/markdown/demo01.gif)
***
